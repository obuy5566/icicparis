using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ColTT : MonoBehaviour
{
    [SerializeField] GameObject g;
    [SerializeField] float moveSpeed;
    Vector3 targetVec;
    public List<Obs> ttt = new List<Obs>();
    public float rad;
    public float probeLength;
    float minMag = 2000f;
    public float rotAngle;
    public float turnAngle = 0f;

    Obs finalOA = null;
    float finalNearestMag = 0f;
    float finalBothMag = 0f;
    Vector3 finalVec = Vector3.zero;

    Predicate<Obs> jud;

    private void Start()
    {
        jud = JudgeForCol;
    }

    void FixedUpdate()
    {
        MoveToTarget();
    }

    void MoveToTarget()
    {
        targetVec = (g.transform.position - transform.position).normalized;

        var obsTarget = ttt.Find(jud);

        if (obsTarget != null) 
        {
            Avoidance();
        }
        else
        {
            minMag = 200f;
            transform.forward = targetVec;
        }

        transform.position += transform.forward * moveSpeed * Time.deltaTime;
    }

    private void Avoidance()
    {
        float turnAngle = Mathf.Clamp((finalBothMag - minMag), -rotAngle, rotAngle);
        Debug.Log($"{turnAngle}+1st angle");
        Vector3 vcross = Vector3.Cross(transform.forward, finalVec);  //left right judgement
        if (vcross.y > 0f) { turnAngle = -turnAngle; }//left right judgement
        if (Vector3.Dot(finalVec, transform.forward) < 0.30f)
        {
            turnAngle = -turnAngle * 0.78f;
        }
        transform.Rotate(new Vector3(0f, turnAngle, 0f));
    }

    bool JudgeForCol(Obs obs)
    {
        float probeAndtttRad = probeLength + obs.oRad;
        float obsMag = (obs.transform.position - transform.position).magnitude;
        if(obsMag > probeAndtttRad) return false;
        Vector3 ObsVec = (obs.transform.position - transform.position).normalized;
        float dotFBtwObs = Vector3.Dot(ObsVec, transform.forward);
        Debug.Log($"{obs.name} + {dotFBtwObs}+the dot product");
        if (dotFBtwObs < 0.04f) return false;
        float obsProjMag = obsMag * dotFBtwObs;
        float nearestMag = Mathf.Sqrt(obsMag * obsMag - obsProjMag * obsProjMag);
        float bothMag = rad + obs.oRad;
        if(obsMag < minMag)
        {
            minMag = obsMag;
            finalOA = obs;
            finalNearestMag = nearestMag;
            finalBothMag = bothMag;
            finalVec = ObsVec;
        }
        return nearestMag < bothMag;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, rad);
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * probeLength);
    }
}
