﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obs : MonoBehaviour
{
    public float oRad = 1f;

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, oRad);
    }
}
